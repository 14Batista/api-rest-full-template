  
# Your API Name 🚀

This is my idea to start an API in **Node JS**, **Express**, and **Sequelize**.

## Key Features

- **Swagger Documentation:** We use the OpenAPI 3.0 specification to document our API. You can access the documentation at [http://localhost:4000/docs](http://localhost:4000/docs) while running the local server.

- **Database Management with Sequelize:** We use Sequelize to manage the database connection with an easy-to-understand and maintainable structure, with the flexibility to work with multiple databases in different dialects. We primarily focus on using MySQL as our database dialect.

## Requirements

To use this API, you need to have at least version **20.13** of Node.js installed.

## Installation

You can install version **20.13** of Node.js using NVM:

```bash
nvm install 20.13 --reinstall-packages-from=node
```
Then, use this version:
```bash
nvm use 20.13
```

To install dependencies using **npm**, run the following command:
```bash
npm install
```
 - **Development**
To run the project in a **development** environment, execute the command:
	```bash
	npm run dev
	```
	Also, make sure to create a file in the `src/.env` path with the following environment variable:
	

	>**Note:** In this file, you add your customized variables for the **development** environment.

	```plaintext
	ENV=dev
	CUSTOM1=foo
	CUSTOM2=bar
	``` 
-   **Production** To run the project in a **production** environment, execute the command:
	```bash
	npm run start
	```
	 or
	```bash
	npm start
	```
	Make sure **NOT** to add the `ENV=dev` variable in the configurations of your **production** server:
	
	```plaintext
	CUSTOM1=foo
	CUSTOM2=bar
	``` 