import { secrets } from '#@/components/constants.js'
import jwt from 'jsonwebtoken'

export default () => (req, res, next) => {
    let decodedToken = {}
    try {
        const authorization = req.get('authorization')
        const secret = secrets.tokenSecret
        let token = null

        if (authorization && authorization.toLocaleLowerCase().startsWith('bearer')) {
            token = authorization.split(' ')[1]
        }

        decodedToken = token ? jwt.verify(token, secret) : null
        if (!token || !decodedToken.id) {
            return res.status(401).json({ resp: 'Missing auth token' })
        }
        req.userToken = decodedToken
        next()
    } catch (error) {
        return res.status(422).json({ resp: error })
    }
}
