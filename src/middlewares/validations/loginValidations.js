import { check, validationResult } from 'express-validator'
import validator from './validator.js'

const testValidations = () => {
    return [
        check('user').notEmpty(),
        check('pass').notEmpty(),
        validator((validationResult))
    ]
}

export default testValidations
