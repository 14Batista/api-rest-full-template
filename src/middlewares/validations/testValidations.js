import { check, validationResult } from 'express-validator'
import validator from './validator.js'

const testValidations = () => {
    return [
        check('number').notEmpty().withMessage('number issss required'),
        validator((validationResult))
    ]
}

export default testValidations
