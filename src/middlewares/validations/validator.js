const validator = (validationResult) => (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        const errorList = errors.array().map(error => {
            const errorMsg = error.msg === 'Invalid value' ?
                error.param + ' is required!' : error.msg

            return { param: error.param, msg: errorMsg }
        })
        // console.log('Errors: ', errorList)
        return res.status(422).json({ errors: errorList })
    }
    next()
}

export default validator
