// eslint-disable-next-line no-unused-vars
class Schema {
    constructor(properties) {
        this.type = 'object'
        this.properties = properties
    }
}

// const SignIn = new Schema({
//     email: { type: "string" },
//     password: { type: "string" }
// });
const TestRequest = new Schema({
    number: { type: 'string' }
})
const TestResponse = new Schema({
    word: { type: 'string' }
})

export default {
    // SignIn
    TestRequest,
    TestResponse
}
