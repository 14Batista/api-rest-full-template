
/**
 * @swagger
 * /test/:
 *   get:
 *     description: Return helow word for test the API
 *     responses:
 *       200:
 *         description: Success respnse
 */

/**
 * @swagger
 * /test/:
 *   post:
 *     summary: Test endpoint
 *     description: Endpoint to test validation and filtering.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/TestRequest'
 *     responses:
 *       200:
 *         description: Success response
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/TestResponse'
 */
