export default [{
    name: 'Auth',
    description: 'Login and Register'
}, {
    name: 'Users',
    description: 'Operations about user'
}, {
    name: 'Data',
    description: 'Operations about data'
}, {
    name: 'Membership',
    description: 'Operations about membership'
}, {
    name: 'Partners',
    description: 'Operations about partners'
}]
