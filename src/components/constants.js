/* eslint-disable camelcase */

const mainDbConfig = {
    host: process.env.MAIN_DB_HOST || null,
    name: process.env.MAIN_DB_NAME || null,
    user: process.env.MAIN_DB_USER || null,
    pass: process.env.MAIN_DB_PASS || null
}

const secrets = {
    tokenSecret: process.env.SECRET_TOKEN || null
}

export {
    mainDbConfig, secrets
}
