const pagination = {
    set: (params) => {
        let { shortBy = '', take = 10, page = 1 } = params
        page = parseInt(page)
        take = parseInt(take)

        let from = page * take - take
        return ({ page, take, from, shortBy })
    },
    response: (pg, data, count) => {
        const page = pg.page
        const perPage = pg.take

        const totalPages = Math.ceil(count / pg.take)

        return {
            page, perPage, total: count, totalPages, data
        }
    }
}

export default pagination
