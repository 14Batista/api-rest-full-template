export default (sequelize, type) => {
    const User = sequelize.define('User', {
        user_Id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: type.STRING(100),
        last_Name: type.STRING(100),
        email: {
            type: type.STRING(100),
            unique: true
        },
        phone: type.STRING(100),
        image: type.BLOB,
        sts: {
            type: type.BOOLEAN,
            defaultValue: true
        }
    }, {
        tableName: 'users',
        timestamps: false
    })

    // User.associate = models => {
    //     User.belongsTo(models.Role, { foreignKey: 'role_id', as: 'role' })
    //     User.belongsTo(models.Location, { foreignKey: 'location_id', as: 'location' })
    // }

    return User
}

