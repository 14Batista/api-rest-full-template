import Sequalize from 'sequelize'
import { mainDbConfig } from '#@/components/constants.js'

// Require models
import Models from './syncModels.js'

// Require Associations
// import Associations from './associations.js'

// Initilize Conection
const sequelize = new Sequalize(mainDbConfig.name, mainDbConfig.user, mainDbConfig.pass, {
    host: mainDbConfig.host,
    port: mainDbConfig.port,
    dialect: 'mysql'
    // logging: false // Show query logs
})

// Initialization instance of models
const initilizedModels = Models(sequelize, Sequalize)

// Setup models associations
Object.keys(initilizedModels).forEach(key => {
    if ('associate' in initilizedModels[key]) {
        initilizedModels[key].associate(initilizedModels)
    }
})

// Sync tables to Database
let alter = false // Efect changes to tables

sequelize.sync({
    force: false,
    alter
})
    .then(() => {
        console.log(alter ? 'Tables Sync' : 'Tables Ok')
    })
    .catch((error) => {
        const err = JSON.parse(JSON.stringify(error)).name
        process.env.ENV === 'dev' ?
            console.log('Can not connect to DB...', err) :
            console.log('Can not connect to DB...')
    })

// Export Models instance
export default initilizedModels
