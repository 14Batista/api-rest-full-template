// Require models
import User from './models/users.js'

export default (sequelize, Sequalize) => {
    return {
        User: User(sequelize, Sequalize)
    }
}
