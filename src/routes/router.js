// const router = require('express').Router()
import { Router } from 'express'
const router = Router()

// Import middlewars
// ...

// Import files
import v1 from './v1/routerV1.js'

// Use routers
router.use('/v1', v1)

// Main Index
router.get('/', (req, res) => {
    const protocol = req.protocol
    const host = req.hostname
    const port = process.env.PORT || 4000

    const fullUrl = `${protocol}://${host}:${port}/docs`

    res.status(200).json({
        apiName: 'Template for API-REST',
        activeVersion: 'V1',
        poweredBy: '14Batista',
        documentation: fullUrl
    })
})

export default router
