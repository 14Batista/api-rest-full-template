
import { Router } from 'express'
import jwt from 'jsonwebtoken'

import { secrets } from '#@/components/constants.js'
import loginValidations from '#@/middlewares/validations/loginValidations.js'

// Archivo para las validaciones de la ruta /test como Middleware
import validateToken from '#@/middlewares/validateToken.js'

// Importar modelo de BD desde el archivo de connecion de la BD
// const { User } = require('../../components/dbs/main/conections')

const router = Router()

router.get('/1nf0t0k', (req, res) => {
    // Ejemplo de consunlta a la BD usando Sequalize
    // try {
    //     const data = await User.findOne({
    //         where: { user_Id: req.query.id }
    //     })

    //     res.status(200).json({ info: data })
    // } catch (error) {
    //     return res.status(422).json({ error });
    // }

    res.send('Informacion del token')
})

router.get('/testToken', validateToken(), (req, res) => {
    return res.status(200).json({ resp: 'Informacion del token', userInfo: req.userToken })
})

router.post('/login', loginValidations(), (req, res) => {
    const { user, pass } = req.body

    // Funciones para Longin en DB
    if (user !== '14Batista' || pass !== '123daniell456') {
        return res.status(422).json({ resp: 'User and password are incorrect' })
    }
    const userinfo = { id: 'us3r1d809809', name: 'Danny', lastName: 'Crispin' }

    // Creacion del token
    const secret = secrets.tokenSecret
    const payload = {
        id: userinfo.id,
        name: userinfo.name,
        lastName: userinfo.lastName
    }

    // const token = jwt.sign(payload, secret, { expiresIn: '5m' })
    const token = jwt.sign(payload, secret)
    delete userinfo.id

    return res.status(200).json({ userinfo, token })
})

export default router
