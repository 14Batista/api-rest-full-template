// const router = require('express').Router()
import { Router } from 'express'
const router = Router()

// Import middlewars
// ...

// Import files
import test from './test.js'
import login from './auth/login.js'

// Authentication Routers
router.use('/auth/', login)

// Use routes
router.use('/', test)

export default router
