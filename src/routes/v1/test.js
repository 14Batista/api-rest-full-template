
import { Router } from 'express'
const router = Router()

// Archivo que contiene la DOCUMENTACION de las RUTAS de este docuemnto

// Archivo para las validaciones de la ruta /test como Middleware
import testValidations from '#@/middlewares/validations/testValidations.js'

// Importar modelo de BD desde el archivo de connecion de la BD
// const { User } = require('../../components/dbs/main/conections')

router.get('/test', (req, res) => {
    // Ejemplo de consunlta a la BD usando Sequalize
    // try {
    //     const data = await User.findOne({
    //         where: { user_Id: req.query.id }
    //     })

    //     res.status(200).json({ info: data })
    // } catch (error) {
    //     return res.status(422).json({ error });
    // }

    res.send('Hola Mundo V1')
})

router.post('/test', testValidations(), (req, res) => {
    const { number } = req.body

    console.log(req.body)
    const list = [{
        name: 'uno',
        number: '1'
    }, {
        name: 'dos',
        number: '2'
    }, {
        name: 'tres',
        number: '3'
    }, {
        name: 'cuatro',
        number: '4'
    }, {
        name: 'cinco',
        number: '5'
    }]

    const filtered = list.filter(item => item.number === number)
    let word = filtered.length > 0 ? filtered[0].name : 'no encontrado'

    return res.status(200).json({ word })
})

export default router
