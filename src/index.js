// Requires
import express, { json, urlencoded } from 'express'
import morgan from 'morgan'

import router from './routes/router.js'
import swagger from './components/swagger/swagger.js'

// import './components/dbs/main/conection.js'

// Aplication
const app = express()

// Settings
app.set('appName', 'Application Name')
app.set('port', process.env.PORT || 4000)

// Middlewares
swagger(app)

// Morgan
if (process.env.ENV === 'dev') app.use(morgan('dev'))

app.use(json())
app.use(urlencoded({ extended: false }))

// Router
app.use('/', router)

// Server
app.listen(app.get('port'), () => {
    // Ejemplo de leer variables de entorno en el archivo src/.env
    const mode = process.env.ENV === 'dev' ? 'Development' : 'Production'

    console.log(app.get('appName'))
    console.log(`Running on port: ${app.get('port')}, in mode: ${mode}`)
})
